TEX=pdflatex
OBJS=document/Document.pdf Interview_Session_2/Interview_Session_2_Questions.pdf

all: $(OBJS)

ifeq ($(shell hash latexmk 2>/dev/null;echo $$?),0)
%.pdf: FORCE
	latexmk -cd -pdf "$(@:pdf=tex)"

clean:
	for pdf in $(OBJS); do latexmk -cd -c "$$pdf"; done

FORCE:
else
%.pdf: %.tex
	cd "$(dirname "$<")" &&  pdflatex $< && pdflatex $<
endif

