\subsection*{Use Case 1: User login}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Author                  & Erin van der Veen \\\hline
    Description             & The Director of any of the departments logs into the system. \\\hline
    Source                  & Interview Session 2\\\hline
    Version                 & 2.0 (Focused) \\\hline
    Basic course of events  & \begin{smallenumerate}
                                \item Program prompts for credentials.
                                \item Director enters credentials.
                                \item \label{item:uc-1-authserverconnect} Program sends the credentials to the authentication server.
                                \item \label{item:uc-1-authserverreponse} Authentication server responds with 'Authentication succeeded'.
                                \item Program shows home screen based on the credentials provided by the Director.
                              \end{smallenumerate}\\\hline
    Alternative paths       & \begin{smallenumerate}[\getrefnumber{item:uc-1-authserverreponse}]
                                \item Authentication server responds with 'Authentication failed'.
                                \item Program displays error message.
                                \item Programs prompts for credentials.
                              \end{smallenumerate}\\\hline
    Exception paths         & \begin{smallenumerate}[\getrefnumber{item:uc-1-authserverconnect}]
                                \item Authentication server is not available.
                                \item Program displays error message.
                              \end{smallenumerate}\\\hline
    Triggers                & Member of Technical Staff runs program. \\ \hline
    Assumptions             & The Director possesses credentials. \\ \hline
    Preconditions           & The Director opens the program. \\ \hline
    Postconditions          & The Director is logged into the system. \\ \hline
    Related business rules  & \\ \hline
\end{longtable}

\subsubsection*{Scenario Basic course of events}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description & Director logs in succesfully.\\\hline
    Actor       & Financial Director (Mark), Program, Authentication server \\\hline
    BCoE        & \begin{smallenumerate}
                    \item The program prompts Mark for his credentials.
                    \item Mark enters his user name and password.
                    \item The program sends the given credentials to the Authentication server.
                    \item The authentication server sends 'Authentication succeeded' to the program.
                    \item The program shows the home screen designed for the Financial Director.
                  \end{smallenumerate} \\\hline
\end{longtable}

\subsubsection*{Scenario Alternative path 1}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description & The Director fails to enter his credentials correctly and is prompted for them again. \\\hline
    Actor       & Financial Director (Mark), Program, Authentication server \\\hline
    BCoE        & \begin{smallenumerate}
                    \item The authentication server sends 'Authentication failed' to the program.
                    \item The program displays error message.
                    \item The program prompts for credentials.
                  \end{smallenumerate} \\\hline
\end{longtable}

\subsubsection*{Scenario Exception path 1}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description & The Director fails to enter his credentials correctly but the program cannot connect to the authentication server. \\\hline
    Actor       & Financial Director (Mark), Program, Authentication server \\\hline
    BCoE        & \begin{smallenumerate}
                    \item The program does not receive a message from the authentication server within the time limit.
                    \item The program displays error message.
                  \end{smallenumerate} \\\hline
\end{longtable}

\subsubsection*{Domain Model}
See \autoref{fig:uc1-orm}.


\begin{figure}[h]
    \centering
    \begin{tikzpicture}[orm,node distance=5mm]
        \entity (credentials) {Credentials};
    
        \binary[left=of credentials.north west,anchor=east,yshift=5mm,unique=2] (f1) {};
        \value[left=of f1.west,anchor=east] (password) {Password};
        \plays (credentials) -- (f1.east); \plays (f1) -- (password);
    
        \binary[left=of credentials.south west,anchor=east,yshift=-5mm,unique=2] (f2) {};
        \entity[left=of f2.west,anchor=east] (authserver) {Authentication server};
        \plays (credentials) edge[mandatory] (f2.east); \plays (f2) -- (authserver);
        
        \binary[right=of credentials.north east,anchor=west,yshift=5mm,unique=1, unique=2] (f3) {};
        \value[right=of f3.east,anchor=west] (username) {User name};
        \plays (credentials) -- (f3.west); \plays (f3) -- (username);
        
        \binary[right=of credentials.south east,anchor=west,yshift=-5mm, unique=1, unique=2] (f4) {};
        \entity[right=of f4.east,anchor=west] (user) {User};
        \plays (credentials) -- (f4.west); \plays (user) edge[mandatory] (f4);

        \node[constraint=external, above=15mm of credentials.north] (c1) {u};
        \limits (f1.one north) -- (c1); \limits (f3.two north) -- (c1);
    \end{tikzpicture}
    \caption{Domain Model of Use Case 1}
    \label{fig:uc1-orm}
\end{figure}

