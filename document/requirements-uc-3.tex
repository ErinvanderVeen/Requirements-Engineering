\subsection*{Use Case 3: Request team setups from iMA}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Author                  & Oussama Danba \\ \hline
    Description             & In this Use Case the program requests the improvements that the algorithm from iMA comes up with. \\ \hline
    Source                  & Interview Session 1 \\ \hline
    Version                 & 2.0 (Focused) \\ \hline
    Basic course of events  & \begin{smallenumerate}
                                \item Program receives request for team setups with given parameters.
                                \item Program makes a customized API request to the iMA servers.
                                \item \label{item:uc-3-limit} iMA servers check that the monthly request limit is not exceeded and returns data in known format.
                                \item \label{item:uc-3-discard} Program receives data in known format and stores it in the local database.
                              \end{smallenumerate}\\\hline
    Alternative paths       & \begin{smallenumerate}[\getrefnumber{item:uc-3-limit}]
                                \item iMA servers note that the monthly request limit is exceeded and respond with a request limit error code and do not provide team setups.
                                \item iMA servers send an automated mail to the Financial Director informing him that the monthly request limit has been reached.
                                \item Program receives request limit error code and quietly stops executing the new requests.
                              \end{smallenumerate}\\\hline
    Exception paths         & \begin{smallenumerate}[\getrefnumber{item:uc-3-limit}]
                                \item iMA servers do not respond within time limit.
                                \item Program does not receive response and as a result does not update local database and goes back to start of Basic Course of Events after an amount of time.
                              \end{smallenumerate}\\
                            & \begin{smallenumerate}[\getrefnumber{item:uc-3-discard}]
                                \item Local database does not respond.
                                \item Program communicates with the Operating System to create a log file with time and date of error.
                                \item Program receives message from Operating System that the file has been created and stops running.
                              \end{smallenumerate}\\\hline
    Triggers                & The Financial Director changes the team parameters. \\ \hline
    Assumptions             & Both the program and iMA servers know in what format they need to communicate and the program has a working Internet connection. \\ \hline
    Preconditions           & Team parameters have been set. \\ \hline
    Postconditions          & Program saved new team setups in local database. \\ \hline
    Related business rules  & \ref{rule:player-range} \\ \hline
\end{longtable}

\subsubsection*{Scenario Basic Course of Events}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description             & Team parameters have changed and as a result the team setups need to be updated. \\ \hline
    Actor                   & Program \\ \hline
    BCoE                    & \begin{smallenumerate}
                                \item Program is notified that the parameters have changed because the Financial Director set the budget to \euro50M.
                                \item Program makes a request to iMA for team setups with budget set to \euro50M.
                                \item iMA checks if the program has gone over the monthly limit. This has not happened and thus returns the team setups.
                                \item Program receives the team setups from iMA and stores them in the local database.
                              \end{smallenumerate} \\ \hline
\end{longtable}

\subsubsection*{Scenario alternative path 1}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description             & Team parameters have changed and as a result the team setups need to be updated but the monthly request limit has been reached. \\ \hline
    Actor                   & Program \\ \hline
    BCoE                    & \begin{smallenumerate}
                                \item Program is notified that the parameters have changed because the Financial Director set the budget to \euro50M.
                                \item Program makes a request to iMA for team setups with budget set to \euro50M.
                                \item iMA checks if the program has gone over the monthly limit. This has happened and thus responds with a request limit error code.
                                \item iMA sends an automated mail to the Financial Director.
                                \item Program receives request limit error code and stops executing the new requests.
                              \end{smallenumerate} \\ \hline
\end{longtable}

\clearpage
\subsubsection*{Scenario exception path 1}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description             & Team parameters have changed and as a result the team setups need to be updated but the iMA servers do not respond. \\ \hline
    Actor                   & Program \\ \hline
    BCoE                    & \begin{smallenumerate}
                                \item Program is notified that the parameters have changed because the Financial Director set the budget to \euro50M.
                                \item Program makes a request to iMA for team setups with budget set to \euro50M.
                                \item Program does not receive response within the time limit.
                                \item Program does not update local database and starts over after an amount of time.                                
                              \end{smallenumerate} \\ \hline
\end{longtable}

\subsubsection*{Scenario exception path 2}
\begin{longtable}{|L|p{11cm}|}
    \hline
    Description             & Team parameters have changed and as a result the team setups need to be updated but the local database does not respond. \\ \hline
    Actor                   & Program \\ \hline
    BCoE                    & \begin{smallenumerate}
                                \item Program is notified that the parameters have changed because the Financial Director set the budget to \euro50M.
                                \item Program makes a request to iMA for team setups with budget set to \euro50M.
                                \item iMA checks if the program has gone over the monthly limit. This has not happened and thus returns the team setups.
                                \item Program receives the team setups from iMA and attempts to store them in the local database but it does not respond.
                                \item Program communicates to Operating System and creates a log file indicating that the local database is not available with the current date and time.
                                \item Program receives a message from the Operating System that the log file was successfully created and the program stops running.
                              \end{smallenumerate} \\ \hline
\end{longtable}

\subsubsection*{Domain Model}
See \autoref{fig:uc3-orm}.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}[orm,node distance=5mm]

        \entity (parameters) {Parameters};

        \binary[left= of parameters, unique=1] (f1) {};
        \entity[left= of f1] (request) {Request};
        \plays (parameters) -- (f1); \plays (request) edge[mandatory] (f1);

        \binary[left= of request, unique=1-2] (f2) {};
        \entity[left= of f2, power=Team Setup] (player) {Player};
        \plays (player-power) edge[mandatory] (f2); \plays (request) edge[mandatory] (f2);

        \binary[left= of player-power, unique=1-2] (f4) {};
        \entity[left= of f4] (database) {Database};
        \plays (database) -- (f4); \plays (player-power) edge[mandatory] (f4);

        \binary[above= 1cm of f1, unique=1-2] (f5) {};
        \entity[left= of f5] (cplayer) {Current Player};
        \plays (parameters) -- (f5.east); \plays (cplayer) -- (f5);
        \draw[suptype] (cplayer) to (player);

        \node[constraint=external, below left= of player-power,shape=ellipse,inner sep=1mm] (c1) {14 \dots 18};
        \limits (c1) -- (player-power);
    \end{tikzpicture}
    \caption{Domain Model of Use Case 3}
    \label{fig:uc3-orm}
\end{figure}

